from os.path import dirname, abspath
from asdf import util
from .type import EPSFType, FittableImageModelType



class EPSFExtension(asdf.AsdfExtension):
    @property
    def types(self):
        return [FittableImageModelType, EPSFType]

    @property
    def tag_mapping(self):
        return [('tag:https://gitlab.com/variaatio:custom/epsf/',
                 'https://gitlab.com/variaatio/schemas/custom/epsf/{tag_suffix}')]

    @property
    def url_mapping(self):
        return [('https://gitlab.com/variaatio/schemas/custom/epsf/',
                 util.filepath_to_url(abspath(dirname(__file__)+
                  '/../schemas'))+'/{url_suffix}.yaml')]

