from asdf.types import CustomType, ExtensionTypeMeta
from asdf.yamlutil import tagged_tree_to_custom_tree, custom_tree_to_tagged_tree
from model_asdf.type import BoundType, BoundBoxType, ParameterType
from model_asdf.type import ModelType, Fittable2DModelType
from photutils.psf import EPSFModel, FittableImageModel


class EPSF_ASDF_Type(CustomType):
    organization = 'https://gitlab.com/variaatio'
    standard = 'custom/epsf'


class FittableImageModelType(EPSF_ASDF_Type):
    name = 'fit_img_model'
    version = (0, 0, 1)
    types = [FittableImageModel]

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        if isinstance(node, FittableImageModel):
            tree['data'] = custom_tree_to_tagged_tree(node.data, ctx)
            tree['flux'] = custom_tree_to_tagged_tree(node.flux, ctx)
            tree['x_0'] = custom_tree_to_tagged_tree(node.x_0, ctx)
            tree['y_0'] = custom_tree_to_tagged_tree(node.y_0, ctx)
            if node.origin is None:
                tree['origin'] = None
            else:
                tree['origin'] = {'x': node.origin[0],
                                  'y': node.origin[1]}
            tree['normalization_status'] = node.normalization_status
            tree['normalization_correction'] = node.normalization_correction
            tree['oversampling'] = {'x': node.oversampling[0],
                                    'y': node.oversampling[1]}
            tree['fill_value'] = node.fill_value
            return tree
        else:
            raise TypeError('not a FittableImageModel')

    @classmethod
    def from_tree(cls, tree, ctx):
        worktree = tree.copy()
        worktree['origin'] = (worktree['origin']['x'],
                              worktree['origin']['y'])
        worktree['oversampling'] = (worktree['oversampling']['x'],
                                    worktree['oversampling']['y'])
        # sending false to normalization to prevent
        # double normalization affecting model
        worktree['normalization'] = False
        normalization_status = worktree.pop('normalization_status')
        data = worktree.pop('data')
        model = FittableImageModel(data, **worktree)
        # copy in the correct value for normalization status
        model._normalization_status = normalization_status
        return model
    @classmethod
    def FIM2tree(cls, node, ctx):
        tree = {}
        tree.update(Fittable2DModelType.F2D2tree(node, ctx))
        tree['data'] = custom_tree_to_tagged_tree(node.data, ctx)
        tree['fill'] = node.fill_value
        tree['norm_stat'] = node.normalization_status
        tree['oversampling'] = custom_tree_to_tagged_tree(
            node.oversampling, ctx)
        tree['norm_const'] = node.normalization_constant
        tree['norm_corr'] = node.normalization_correction
        tree['interpolatorargs'] = custom_tree_to_tagged_tree(
            node.interpolator_kwargs, ctx)
        tree['origin'] = custom_tree_to_tagged_tree(node.origin,ctx)
        return tree
class EPSFModelType(EPSF_ASDF_Type):
    name = 'epsfmodel'
    version = (0, 0, 1)
    types = [EPSFModel]

    @classmethod
    def to_tree(cls, node, ctx):
        if isinstance(node, EPSFModel):
            tree = {}
            tree.update(FittableImageModelType.FIM2tree(node, ctx))
            tree['norm_radius'] = node._norm_radius
            tree['shift_val'] = node._shift_val
            return tree
        else:
            raise TypeError('not a EPSFModel')

    @classmethod
    def from_tree(cls, tree, ctx):
        model = EPSFModel.__new__(EPSFModel)
        inittree = {
            'origin': tagged_tree_to_custom_tree(tree.get('origin'), ctx),
            'fill_value': tree.get('fill', 0),
            'oversampling': tagged_tree_to_custom_tree(
                tree.get('oversampling'), ctx)}
        interpolator = tagged_tree_to_custom_tree(tree.get('interpolatorargs'),
                                                  ctx)
        if isinstance(interpolator, dict):
            inittree.update(interpolator)
        data = tagged_tree_to_custom_tree(tree.get('data'), ctx)
        model.__init__(data, **inittree)
        # copy in the correct value for normalization status
        model._normalization_status = tree.get('norm_stat', 2)
        model._normalization_correction = tree.get('norm_corr', 1)
        model._normalization_constant = tree.get('norm_const', 1)
        model._norm_radius = tree.get('norm_radius', 5.5)
        model._shift_val = tree.get('shift_val', 0.5)
        return model
