# epsf-asdf

Extension implementation ASDF serialization and deserialization for 
FittableImageModel and EpsfModel from photutils.

## Install:
python setup.py install

## Uninstall:
pip uninstall epsf-asdf

## usage:
just import asdf and use it normally. On install the extension is hooked to 
asdf-extensions entrypoints. ASDF should automatically know to use the extension
upon encountering any of it's registered types: FittableImageModel and EpsfModel