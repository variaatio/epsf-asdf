from setuptools import setup
setup(
    name='epsf_asdf',
    version="0.0.2",
    packages=['epsf_asdf',],
    install_requires=["asdf","photutils","astropy","model_asdf>=0.0.1"],
    author="Ari Takalo",
    url="https://gitlab.com/variaatio/epsf-asdf",
    classifiers=[
        "License :: OSI Approved :: MIT License"
    ],
    data_files=[('schemas',['schemas/epsf-0.0.1.yaml',
                  'schemas/fit_img_model-0.0.1.yaml']),]
)
